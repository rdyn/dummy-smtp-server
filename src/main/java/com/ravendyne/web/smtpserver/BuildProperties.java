package com.ravendyne.web.smtpserver;

import java.io.InputStream;
import java.util.Properties;

/**
 * Singleton class which loads application build version and returns the result
 * 
 * @author iceman
 *
 */
public class BuildProperties
{
    private static BuildProperties INSTANCE          = new BuildProperties();
    private final String           versionMajorMinor = "0.1";
    private String                 version;

    private BuildProperties()
    {
        try
        {
            Properties configProp = new Properties();
            InputStream in = this.getClass().getResourceAsStream("/version.properties");
            if (in != null)
            {
                configProp.load(in);
                version = versionMajorMinor + "-" + configProp.getProperty("version");
            }
            else
            {
                version = "~";
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static String getVersion()
    {
        return INSTANCE.version;
    }
}
