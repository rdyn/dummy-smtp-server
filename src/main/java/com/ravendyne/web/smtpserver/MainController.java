package com.ravendyne.web.smtpserver;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.subethamail.wiser.WiserMessage;

@Controller
public class MainController
{
    @Autowired
    SimpleSmtpServer smtpServer;

    @RequestMapping("/")
    public String index(Model model)
    {
        model.addAttribute("buildVersion", BuildProperties.getVersion());

        List<MailMessageDetail> messages = new ArrayList<MailMessageDetail>();
        for (WiserMessage message : smtpServer.getMessages())
        {
            MailMessageDetail mailMessageDetail = new MailMessageDetail(message);
            messages.add(mailMessageDetail);
        }
        model.addAttribute("messages", messages);

        return "index";
    }

    @RequestMapping("/clearMessages")
    public String clearMessages(Model model)
    {
        smtpServer.getMessages().clear();

        return "index";
    }
}
