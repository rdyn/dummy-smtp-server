package com.ravendyne.web.smtpserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DummySmtpServerApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(DummySmtpServerApplication.class, args);
    }
}
