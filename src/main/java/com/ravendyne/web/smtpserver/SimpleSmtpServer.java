package com.ravendyne.web.smtpserver;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.subethamail.wiser.Wiser;

public class SimpleSmtpServer extends Wiser
{
    private static Log log = LogFactory.getLog(SimpleSmtpServer.class);

    public SimpleSmtpServer()
    {
        super();
    }

    public void init()
    {
        log.info("Starting SMTP server...");
        this.getServer().setEnableTLS(false);
        this.start();
        log.info("SMTP server started at port [" + this.getServer().getPort() + "]");
    }
    
    public void destroy()
    {
        log.info("Stopping SMTP server...");
        this.stop();
    }
}
