package com.ravendyne.web.smtpserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Enumeration;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.subethamail.wiser.WiserMessage;

public class MailMessageDetail
{
    private String    envelopeSender;
    private String    envelopeReceiver;
    private String    subject;
    private String    content;
    private String    rawContent;
    private String    headers;
    private Address[] messageCc;
    private Address[] messageBcc;
    private Address[] messageTo;

    public MailMessageDetail(WiserMessage message)
    {
        String envelopeSender = message.getEnvelopeSender();
        String envelopeReceiver = message.getEnvelopeReceiver();

        try
        {
            MimeMessage mess = message.getMimeMessage();
            this.envelopeSender = envelopeSender;
            this.envelopeReceiver = envelopeReceiver;
            this.messageCc = mess.getRecipients(RecipientType.CC);
            this.messageBcc = mess.getRecipients(RecipientType.BCC);
            this.messageTo = mess.getRecipients(RecipientType.TO);
            this.subject = mess.getSubject();
            parseMessageContent(mess);
            
            this.headers = "";
            Enumeration allHeaderLines = mess.getAllHeaderLines();
            while(allHeaderLines.hasMoreElements())
            {
                this.headers += allHeaderLines.nextElement() + "\n";
            }
            
            this.rawContent = "";
            InputStream is = mess.getRawInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while( (line = br.readLine()) != null)
            {
                this.rawContent += line + "\n";
            }
            // dummy way
            this.rawContent = this.rawContent.replace("<", "&lt;");
            this.rawContent = this.rawContent.replace(">", "&gt;");
        }
        catch (MessagingException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void parseMessageContent(MimeMessage mess) throws IOException, MessagingException
    {
        Object msgContent = mess.getContent();
        if (msgContent instanceof MimeMultipart)
        {
            processMimeMultipart((MimeMultipart) msgContent);
        }
        else if (msgContent instanceof String)
        {
            this.content = msgContent.toString();
        }
        else
        {
//            System.out.println("this is not a mime message");
            this.content = "<this is not mime message!!>";
        }
    }

    private void processMimeMultipart(MimeMultipart multipart) throws MessagingException, IOException
    {
        for (int j = 0; j < multipart.getCount(); j++)
        {
            BodyPart bodyPart = multipart.getBodyPart(j);
            Object bodyContent = bodyPart.getContent();
//            System.out.println("ContentType: " + bodyPart.getContentType());
//            System.out.println("Content Java class: " + bodyPart.getContent().getClass().getName());

            if (bodyContent instanceof MimeMultipart)
            {
                processMimeMultipart((MimeMultipart) bodyContent);
            }
            else if (bodyPart.isMimeType("text/plain") || bodyPart.isMimeType("text/html"))
            {
                // TODO sanitize HTML before assigning??
                this.content = bodyContent.toString();
            }
            else
            {
                String disposition = bodyPart.getDisposition();

                if (disposition != null)
                {
                    if (disposition.equalsIgnoreCase(Part.ATTACHMENT))
                    {
//                        System.out.println("Disposition: ATTACHMENT");
                        DataHandler handler = bodyPart.getDataHandler();
//                        System.out.println("file name : " + handler.getName());
                    }
                    else if (disposition.equalsIgnoreCase(Part.INLINE))
                    {
//                        System.out.println("Disposition: INLINE");
                        DataHandler handler = bodyPart.getDataHandler();
//                        System.out.println("file name : " + handler.getName());
                    }
                }
                else
                {
//                    System.out.println("Disposition: UNKNOWN");
                    DataHandler handler = bodyPart.getDataHandler();
//                    System.out.println("file name : " + handler.getName());
                }
            }
        }
    }

    public String getEnvelopeSender()
    {
        return envelopeSender;
    }

    public String getEnvelopeReceiver()
    {
        return envelopeReceiver;
    }

    public String getSubject()
    {
        return subject;
    }

    public String getContent()
    {
        return content;
    }

    public String getHeaders()
    {
        return headers;
    }

    public String getRawContent()
    {
        return rawContent;
    }

    public String getMessageCc()
    {
        return Arrays.toString(messageCc);
    }

    public String getMessageBCc()
    {
        return Arrays.toString(messageBcc);
    }

    public String getMessageTo()
    {
        return Arrays.toString(messageTo);
    }
}
