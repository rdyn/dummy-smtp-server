package com.ravendyne.web.smtpserver;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServerConfig
{
	@Value("${smtpserver.port:2025}")
	Integer smtpPort;

	@Value("${smtpserver.hostname:localhost}")
	String smtpHostname;

    @Bean (initMethod="init", destroyMethod="destroy")
    SimpleSmtpServer smtpServer()
    {
        SimpleSmtpServer smtpServer = new SimpleSmtpServer();
        smtpServer.setPort(smtpPort);
        smtpServer.setHostname(smtpHostname);
        return smtpServer;
    }
}
