# About


## What's this?

Dummy SMTP Server (DSS) is just that:

* SMTP server
* and it's dummy

It receives mails sent from your massive software project and
lets you see received emails through a built-in web interface. And that's pretty much it.

On one of my previous projects we used mailcatcher to intercept and display mails
sent from application we were working on. After getting tired of mailcatcher crashing
randomly and often (something to do with versions of Ruby/libraries we were using
for the project) I took an hour and wrote this little puppy.

It's quick'n'dirty approach, it is not feature ritch and leaves a lot to be desired, handled and improved, 
but it does the basic job we needed it to do, and it does not crash ;)




## How to use it



### To run it

Clone this git repository to your workstation and open the project in Spring Tool Suite. Spring-boot-maven-plugin
is added as a dependency to this project, so all you need to do to build runnable self-contained jar is:

* right-click on project
* select Run As -> Maven build...
* in dialog that pops up:
	* enter "package" in "Goal" edit box
	* check "Skip Tests" checkbox (helps when you run maven package at the same time application is running in the background)
	* maybe change name of the run configuration at the top
	* click Apply and then Run

After that, folder `target` under project root will contain file named `dummy-smtp-server-0.0.1-SNAPSHOT.jar`.
If you don't like the name, open pom.xml and change it.

To run DSS from command line, do:

	$ java -jar dummy-smtp-server-0.0.1-SNAPSHOT.jar

If you want to change default web UI port (8080) or SMTP port (2025) or SMTP hostname (localhost), create file named 
`application.properties` with content like this:

	server.port = 8090
	smtpserver.port = 20125
	smtpserver.hostname = myhost

and place it in the same folder where you put `dummy-smtp-server-0.0.1-SNAPSHOT.jar`. Once you start DSS it will
automagically pick up properties from that file and override default one or the ones set in the project's 
`application.properties`



### To interact with it and have some use of it

DSS will not work on port 25 unless you figure out yourself how to run
it with enough privileges inside your chosen OS. That is why default port it listens for
SMTP connections is `2025`.

Configure your application/project to send emails to localhost:2025 (or wherever DSS may be started)
and you are good to go.

Point your browser to http://localhost:8080 (or wherever you deployed DSS) and you will be
presented with its magnificent web interface:

![main interface][main]



Once messages start arriving, you can either refresh the page in browser or click on
"Reload message list" button:

![message list][message_list]



Click on a message of your interest and you'll see that message details at the bottom panel,
conveniently named "Selected message details". HTML tab shows message text or HTML disposition
of it, if there is any:

![HTML view, if any][html_view]



You can also view message headers in "Headers" tab:

![view message headers][headers_view]



and raw message text (without headers) in the, yes you've guessed it right!, "Raw" tab:

![view raw message text][raw_view]



One not implemented feature is viewing of inlined images:

![inline image view not implemented][missing_inline_img]



We didn't need it, but if you do, it should be fairly easy to implement:

* Make MailMessageDetail look for inline image attachments and img tags with src attribute
having string 'cid:' in its value
* decode and store image in internal `HashMap` of images where key is that 'cid:' identifier
(and probably plus something else, or maybe just use UUID)
* replace 'cid' in message text with link to the controller you will implement in next step, i.e. '/inline_image/{key}'
* implement controller with mapping '/inline_image/{key}' and make it return image/whatever content-type by
fetching image from that hash map using provided key
* ta-daaaaaah!!


# Implementation Notes



## MailMessageDetail

This one processes received message and does all heavy lifting of parsing and storing all the message parts.
Well, honestly, the lifting is not that heavy thanks to `org.subethamail.wiser` and `javax.mail.internet.MimeMessage` packages.

Constructor does the job, calls `parseMessageContent()` to handle content, and that one calls
`processMimeMultipart()` for each multipart it finds in the message. If you want to amend message
parsing features or add new ones, `MimeMessageDetail` is the place to do it.

There are `System.out.println` scattered around, feel free to use them or debugger, whatever works the best
at the moment.



## SMTP server bean

`smtpServer` bean is constructed using `@Configuration` class `ServerConfig`.
Bean is then injected where needed using `@Autowired` annotation.

The other (simpler) way would be to annotate `SimpleSmtpServer` with `@Component`, 
and `init()` and `destroy()` methods with `@PostConstruct` and `@PreDestroy`.

In that case, `smtpPort` and `smtpHostname` properties would have to be moved from
`ServerConfig` to `SimpleSmtpServer` class and used in its `init()` method.

Logging in `SimpleSmtpServer` class is redundant since `Wiser` is already doing the same
thing, but it is there just to announce that our bean methods have been executed.



## If you really want to add something to DSS code, then...

...consider using [testmailer](https://bitbucket.org/rdyn/testmailer) that comes as a DSS helper project.

It is, also dummy, mailer program which sends out handful of emails in different formats and shapes.

By default it connects to localhost on port 2025, so you can use it out of the box while testing DSS.

If nothing else, you can use `testmailer` as a copy-paste source of code which sends different kinds
of mail messages.



## Interesting and totally unnecessary feature (because we can)

`BuildProperties` class is there to pick up minor/build version from the `version.properties` file.

This file is created with `hooks/build_version.sh` Bash script. If you are using Git (as we are),
add call to this script in your post-commit and/or post-merge git hook and every time you commit,
new minor/build version will be recorded in `version.properties` file. This file will be picked up
by jar build and whatever is recorded in it as a value of the `version` property, will be displayed
in the UI footer.



# Attribution

* Icon for application from [Iconbug](http://iconbug.com/detail/icon/5863/blue-and-white-mail/)
* Favicons generated by [Realfavicongenerator](http://realfavicongenerator.net/)
* [Twitter Bootstrap 3](http://getbootstrap.com/) used for UI layout
* Javascript and CSS hosted by [cdnjs](https://cdnjs.com/)
* SMTP server is Wiser package from [Voodoodyne's SubEtha](https://github.com/voodoodyne/subetha)
* Built and brought to life with [Spring Boot](http://projects.spring.io/spring-boot/)



[main]: misc/images/01.png
[message_list]: misc/images/02.png
[html_view]: misc/images/03.png
[headers_view]: misc/images/04.png
[raw_view]: misc/images/05.png
[missing_inline_img]: misc/images/06.png

